/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm;

import javafx.event.EventType;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import static ssm.StartupConstants.STYLE_SHEET_UI;

/**
 *
 * @author Randhawa
 */
public class LanguageStage extends Stage {
    Scene scn ;
    String selection;
    public LanguageStage(){
        this.setTitle("Language");
        this.setMinHeight(200);
        this.setMinWidth(400);
        this.getIcons().add(new Image("https://d30y9cdsu7xlg0.cloudfront.net/png/49992-200.png"));
        VBox vbx = new VBox();
        vbx.setMinHeight(200);
        ComboBox cbx = new ComboBox();
        vbx.setAlignment(Pos.CENTER);
        Button btn = new Button("Continue");
        btn.setAlignment(Pos.BOTTOM_CENTER);
        Label lbl = new Label("Select a Language\n");
        lbl.setId("lang-text");
        lbl.setAlignment(Pos.CENTER);
        vbx.getChildren().add(lbl);
        vbx.getChildren().add(cbx);
        vbx.getChildren().add(btn);
        scn = new Scene(vbx);
          scn.getStylesheets().add(STYLE_SHEET_UI);
        
        this.setScene(scn);
        cbx.getItems().add("English");
        cbx.getItems().add("Punjabi");
        cbx.setValue("English");
        //cbx.getItems()

        btn.setOnAction(e ->{
           selection = cbx.getSelectionModel().getSelectedItem().toString();
           System.out.println("selection here is "+selection);
           this.hide();
        });
    }
    public String getSelection(){
        this.showAndWait();
        return selection;
    }
    
}
