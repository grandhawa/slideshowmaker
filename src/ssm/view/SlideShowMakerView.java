package ssm.view;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.logging.Level;
import java.util.logging.Logger;
import javafx.application.Platform;
import javafx.geometry.Pos;
import javafx.geometry.Rectangle2D;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.TextField;
import javafx.scene.control.Tooltip;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.BorderPane;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Pane;
import javafx.scene.layout.VBox;
import javafx.scene.web.WebView;
import javafx.stage.Screen;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import ssm.LanguagePropertyType;
import static ssm.LanguagePropertyType.TOOLTIP_ADD_SLIDE;
import static ssm.LanguagePropertyType.TOOLTIP_EXIT;
import static ssm.LanguagePropertyType.TOOLTIP_LOAD_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_NEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_SAVE_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_VIEW_SLIDE_SHOW;
import static ssm.LanguagePropertyType.TOOLTIP_TITLE;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_DOWN;
import static ssm.LanguagePropertyType.TOOLTIP_MOVE_UP;
import static ssm.LanguagePropertyType.TOOLTIP_REMOVE_SLIDE;
import static ssm.StartupConstants.CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_EDITOR_PANE;
import static ssm.StartupConstants.CSS_CLASS_SLIDE_SHOW_EDIT_VBOX;
import static ssm.StartupConstants.CSS_CLASS_VERTICAL_TOOLBAR_BUTTON;
import static ssm.StartupConstants.ICON_ADD_SLIDE;
import static ssm.StartupConstants.ICON_REMOVE_SLIDE;
import static ssm.StartupConstants.ICON_EXIT;
import static ssm.StartupConstants.ICON_LOAD_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_NEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_SAVE_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_VIEW_SLIDE_SHOW;
import static ssm.StartupConstants.ICON_MOVE_UP;
import static ssm.StartupConstants.ICON_MOVE_DOWN;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.StartupConstants.STYLE_SHEET_UI;
import static ssm.StartupConstants.ICON_TITLE;
import static ssm.StartupConstants.PATH_BASE_CSS;
import static ssm.StartupConstants.PATH_BASE_HTML;
import static ssm.StartupConstants.PATH_BASE_JS;
import static ssm.StartupConstants.PATH_SITES;
import ssm.controller.FileController;
import ssm.controller.SlideShowEditController;
import ssm.model.Slide;
import ssm.model.SlideShowModel;
import ssm.error.ErrorHandler;
import ssm.file.SlideShowFileManager;

/**
 * This class provides the User Interface for this application,
 * providing controls and the entry points for creating, loading, 
 * saving, editing, and viewing slide shows.
 * 
 * @author McKilla Gorilla & _____________
 */
public class SlideShowMakerView {

    // THIS IS THE MAIN APPLICATION UI WINDOW AND ITS SCENE GRAPH
    Stage primaryStage;
    Scene primaryScene;

    // THIS PANE ORGANIZES THE BIG PICTURE CONTAINERS FOR THE
    // APPLICATION GUI
    BorderPane ssmPane;

    // THIS IS THE TOP TOOLBAR AND ITS CONTROLS
    FlowPane fileToolbarPane;
    Button newSlideShowButton;
    Button loadSlideShowButton;
    Button saveSlideShowButton;
    Button viewSlideShowButton;
    Button exitButton;
    Label titleLabel;
    TextField titleText;
    //Button titleBtn;
    HBox titleBox;
    // WORKSPACE
    HBox workspace;

    // THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
    VBox slideEditToolbar;
    Button addSlideButton;
    Button removeSlideButton;
    public Button moveUpButton;
    public Button moveDownButton;
    
    // AND THIS WILL GO IN THE CENTER
    ScrollPane slidesEditorScrollPane;
    VBox slidesEditorPane;

    // THIS IS THE SLIDE SHOW WE'RE WORKING WITH
    SlideShowModel slideShow;

    // THIS IS FOR SAVING AND LOADING SLIDE SHOWS
    SlideShowFileManager fileManager;

    // THIS CLASS WILL HANDLE ALL ERRORS FOR THIS PROGRAM
    private ErrorHandler errorHandler;

    // THIS CONTROLLER WILL ROUTE THE PROPER RESPONSES
    // ASSOCIATED WITH THE FILE TOOLBAR
    private FileController fileController;
    
    // THIS CONTROLLER RESPONDS TO SLIDE SHOW EDIT BUTTONS
    private SlideShowEditController editController;

    public boolean slideSelected = true;
    /**
     * Default constructor, it initializes the GUI for use, but does not yet
     * load all the language-dependent controls, that needs to be done via the
     * startUI method after the user has selected a language.
     */
    public SlideShowMakerView(SlideShowFileManager initFileManager) {
	// FIRST HOLD ONTO THE FILE MANAGER
	fileManager = initFileManager;
	
	// MAKE THE DATA MANAGING MODEL
	slideShow = new SlideShowModel(this);

	// WE'LL USE THIS ERROR HANDLER WHEN SOMETHING GOES WRONG
	errorHandler = new ErrorHandler(this);
        
        //initialize effects
    }

    // ACCESSOR METHODS
    public SlideShowModel getSlideShow() {
	return slideShow;
    }

    public Stage getWindow() {
	return primaryStage;
    }

    public ErrorHandler getErrorHandler() {
	return errorHandler;
    }
    
    public FileController getFileController(){
        return fileController;
    }

    /**
     * Initializes the UI controls and gets it rolling.
     * 
     * @param initPrimaryStage The window for this application.
     * 
     * @param windowTitle The title for this window.
     */
    public void startUI(Stage initPrimaryStage, String windowTitle) {
	// THE TOOLBAR ALONG THE NORTH
	initFileToolbar();

        // INIT THE CENTER WORKSPACE CONTROLS BUT DON'T ADD THEM
	// TO THE WINDOW YET
	initWorkspace();

	// NOW SETUP THE EVENT HANDLERS
	initEventHandlers();

	// AND FINALLY START UP THE WINDOW (WITHOUT THE WORKSPACE)
	// KEEP THE WINDOW FOR LATER
	primaryStage = initPrimaryStage;
	initWindow(windowTitle);
    }

    // UI SETUP HELPER METHODS
    private void initWorkspace() {
	// FIRST THE WORKSPACE ITSELF, WHICH WILL CONTAIN TWO REGIONS
	workspace = new HBox();
        
        
	
	// THIS WILL GO IN THE LEFT SIDE OF THE SCREEN
	slideEditToolbar = new VBox();
	slideEditToolbar.getStyleClass().add(CSS_CLASS_SLIDE_SHOW_EDIT_VBOX);
	addSlideButton = this.initChildButton(slideEditToolbar,		ICON_ADD_SLIDE,	    TOOLTIP_ADD_SLIDE,	    CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  true);
	removeSlideButton = this.initChildButton(slideEditToolbar, ICON_REMOVE_SLIDE, TOOLTIP_REMOVE_SLIDE, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        moveUpButton = this.initChildButton(slideEditToolbar, ICON_MOVE_UP, TOOLTIP_MOVE_UP, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        slideEditToolbar.setAlignment(Pos.CENTER);
        moveDownButton = this.initChildButton(slideEditToolbar, ICON_MOVE_DOWN, TOOLTIP_MOVE_DOWN, CSS_CLASS_VERTICAL_TOOLBAR_BUTTON,  false);
        //moveDownButton.setAlignment(Pos.CENTER);
        
	// AND THIS WILL GO IN THE CENTER
	slidesEditorPane = new VBox();
        slidesEditorPane.getStyleClass().addAll("pane",CSS_CLASS_SLIDE_EDITOR_PANE);
        
	slidesEditorScrollPane = new ScrollPane(slidesEditorPane);
        slidesEditorScrollPane.getStyleClass().add(CSS_CLASS_SLIDE_EDITOR_PANE);
 
	
	// NOW PUT THESE TWO IN THE WORKSPACE
	workspace.getChildren().add(slideEditToolbar);
	workspace.getChildren().add(slidesEditorScrollPane);
        workspace.getStyleClass().add(CSS_CLASS_SLIDE_EDITOR_PANE);
        slidesEditorPane.setMinHeight(workspace.getHeight());
        
        
    }

    private void initEventHandlers() {
	// FIRST THE FILE CONTROLS
	fileController = new FileController(this, fileManager);
	newSlideShowButton.setOnAction(e -> {
	    fileController.handleNewSlideShowRequest();
	});
	loadSlideShowButton.setOnAction(e -> {
	    fileController.handleLoadSlideShowRequest();
	});
	saveSlideShowButton.setOnAction(e -> {
	    fileController.handleSaveSlideShowRequest();
	});
	exitButton.setOnAction(e -> {
	    fileController.handleExitRequest();
	});
        titleText.textProperty().addListener((observable, oldValue, newValue) -> {
            slideShow.setTitle(newValue);
            fileController.markAsEdited();
        });
        viewSlideShowButton.setOnAction(e ->{
            //make directories and copy required files
            try {
                MakeDir();
                alterFiles();
            } catch (IOException ex) {
                Logger.getLogger(SlideShowMakerView.class.getName()).log(Level.SEVERE, null, ex);
            }
            
            setWebView();
        });

        
	// THEN THE SLIDE SHOW EDIT CONTROLS
	editController = new SlideShowEditController(this);
	addSlideButton.setOnAction(e -> {
	    editController.processAddSlideRequest();
            fileController.markAsEdited();
	});
        removeSlideButton.setOnAction(e -> {
	    editController.processRemoveSlideRequest();
            fileController.markAsEdited();
	});
        moveUpButton.setOnAction(e -> {
	    editController.processMoveUpRequest();
            fileController.markAsEdited();
	});
        moveDownButton.setOnAction(e -> {
	    editController.processMoveDownRequest();
            fileController.markAsEdited();
	});
        
    }

    /**
     * This function initializes all the buttons in the toolbar at the top of
     * the application window. These are related to file management.
     */
    private void initFileToolbar() {
	fileToolbarPane = new FlowPane();
        initTitleBox();
        

        // HERE ARE OUR FILE TOOLBAR BUTTONS, NOTE THAT SOME WILL
	// START AS ENABLED (false), WHILE OTHERS DISABLED (true)
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	newSlideShowButton = initChildButton(fileToolbarPane, ICON_NEW_SLIDE_SHOW,	TOOLTIP_NEW_SLIDE_SHOW,	    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	loadSlideShowButton = initChildButton(fileToolbarPane, ICON_LOAD_SLIDE_SHOW,	TOOLTIP_LOAD_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
	saveSlideShowButton = initChildButton(fileToolbarPane, ICON_SAVE_SLIDE_SHOW,	TOOLTIP_SAVE_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	viewSlideShowButton = initChildButton(fileToolbarPane, ICON_VIEW_SLIDE_SHOW,	TOOLTIP_VIEW_SLIDE_SHOW,    CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, true);
	exitButton = initChildButton(fileToolbarPane, ICON_EXIT, TOOLTIP_EXIT, CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
        fileToolbarPane.getChildren().add(titleBox);
        //fileToolbarPane.getChildren().add(title);
        //titleBtn = initChildButton(fileToolbarPane,ICON_NEXT,TOOLTIP_NEXT_SLIDE,CSS_CLASS_HORIZONTAL_TOOLBAR_BUTTON, false);
    
    }

    private void initWindow(String windowTitle) {
        
        //SET THE WINDOW ICON
        primaryStage.getIcons().add(new Image("https://d30y9cdsu7xlg0.cloudfront.net/png/49992-200.png"));
	//primaryStage.getIcons().add(new Image(getClass().getResourceAsStream("Add.png")));
        // SET THE WINDOW TITLE
	primaryStage.setTitle(windowTitle);

	// GET THE SIZE OF THE SCREEN
	Screen screen = Screen.getPrimary();
	Rectangle2D bounds = screen.getVisualBounds();

	// AND USE IT TO SIZE THE WINDOW
	primaryStage.setX(bounds.getMinX());
	primaryStage.setY(bounds.getMinY());
	primaryStage.setWidth(bounds.getWidth());
	primaryStage.setHeight(bounds.getHeight());

        // SETUP THE UI, NOTE WE'LL ADD THE WORKSPACE LATER
	ssmPane = new BorderPane();
	ssmPane.setTop(fileToolbarPane);	
	primaryScene = new Scene(ssmPane);
	
        // NOW TIE THE SCENE TO THE WINDOW, SELECT THE STYLESHEET
	// WE'LL USE TO STYLIZE OUR GUI CONTROLS, AND OPEN THE WINDOW
	primaryScene.getStylesheets().add(STYLE_SHEET_UI);
	primaryStage.setScene(primaryScene);
	primaryStage.show();
    }
    
    /**
     * This helps initialize buttons in a toolbar, constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public Button initChildButton(
	    Pane toolbar, 
	    String iconFileName, 
	    LanguagePropertyType tooltip, 
	    String cssClass,
	    boolean disabled) {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + iconFileName;
	Image buttonImage = new Image(imagePath);
        Button button = new Button();
	button.getStyleClass().add(cssClass);
	button.setDisable(disabled);
	button.setGraphic(new ImageView(buttonImage));
	Tooltip buttonTooltip = new Tooltip(props.getProperty(tooltip.toString()));
	button.setTooltip(buttonTooltip);
	toolbar.getChildren().add(button);
	return button;   
    }
    
    /**
     * This helps initialize buttons in a Title box , constructing a custom button
     * with a customly provided icon and tooltip, adding it to the provided
     * toolbar pane, and then returning it.
     */
    public void initTitleBox() {
	PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + ICON_TITLE;
	Image buttonImage = new Image(imagePath);
        titleBox = new HBox();
        titleText = new TextField();
        titleLabel = new Label();
        String title = props.getProperty(LanguagePropertyType.DEFAULT_SLIDE_SHOW_TITLE);
        titleLabel.setText(title);
	Tooltip buttonTooltip = new Tooltip(props.getProperty(TOOLTIP_TITLE.toString()));
	titleText.setTooltip(buttonTooltip);
        //titleBtn.setMinSize(10, 10);
	//PUT STUFF IN TITLE HBOX
        titleBox.getChildren().add(titleLabel);
        titleBox.getChildren().add(titleText);
	
    }
    
    /**
     * Updates the enabled/disabled status of all toolbar
     * buttons.
     * 
     * @param saved 
     */
    public void updateToolbarControls(boolean saved) {
	// FIRST MAKE SURE THE WORKSPACE IS THERE
	ssmPane.setCenter(workspace);
	
	// NEXT ENABLE/DISABLE BUTTONS AS NEEDED IN THE FILE TOOLBAR
	saveSlideShowButton.setDisable(saved);
	viewSlideShowButton.setDisable(false);
	
	// AND THE SLIDESHOW EDIT TOOLBAR
	addSlideButton.setDisable(false);
        boolean slideSelected = slideShow.isSlideSelected();
	removeSlideButton.setDisable(!slideSelected);
	moveUpButton.setDisable(!slideSelected);
	moveDownButton.setDisable(!slideSelected);	
    }
    
    public void alterFiles() throws IOException{
        File fileJs = new File(PATH_SITES+slideShow.getTitle()+"/js/Slideshow.js");
        
        String line;
        String jsStr= "";
        String imgName;
        String caption;
        String cap_arr;
        String imgPath;
        String newCapStr = "";
        String newStr = "";
        for(int i = 0; i<slideShow.getSlides().size(); i++){
            imgName = slideShow.getSlides().get(i).getImageFileName();
            imgPath = "\"img/"+imgName+"\"";
            caption = slideShow.getSlides().get(i).getCaption();
            cap_arr = "\""+caption+"\"";
            if(i!=slideShow.getSlides().size()-1){
                imgPath+=",";
                cap_arr+=",";
            }
            newStr +=imgPath;
            newCapStr+=cap_arr;
        }
        try {
            BufferedReader bf = new BufferedReader(new FileReader(fileJs));
            while ((line = bf.readLine()) != null) {
                jsStr+=line;
                jsStr+="\n";
            }
            bf.close();
            jsStr = jsStr.replaceFirst("REPLACE_IMG", newStr);
            jsStr = jsStr.replaceFirst("REPLACE_CAP", newCapStr);
            jsStr = jsStr.replaceFirst("NEW_TITLE", slideShow.getTitle());
        } catch (FileNotFoundException ex) {
            Logger.getLogger(SlideShowMakerView.class.getName()).log(Level.SEVERE, null, ex);
        }
        PrintWriter pw = new PrintWriter(fileJs);
        pw.write(jsStr);
        pw.flush();
        pw.close();
        
    }
    
    public void setWebView(){
         Stage stg = new Stage();
         stg.setTitle(slideShow.getTitle());
         stg.getIcons().add(new Image("https://d30y9cdsu7xlg0.cloudfront.net/png/49992-200.png"));
         WebView view = new WebView();
         String str = PATH_SITES+slideShow.getTitle()+"/index.html";
         File file = new File(str);
         Platform.runLater(new Runnable() {

             @Override
             public void run() {
             view.getEngine().load(file.toURI().toString());
             }
         });
        
         

         view.getEngine().reload();
         Scene scn = new Scene(view);
         stg.setScene(scn);
         stg.show();
    }

    //make folders
    public void MakeDir() throws IOException{
        File file = new File(PATH_SITES+slideShow.getTitle()+"/css"); 
        try {
		if (file.mkdirs()) {
			System.out.println("Directory created");
		} else {
			System.out.println("Directory exists/Failed creating directory");
		}
                file = new File(PATH_SITES+slideShow.getTitle()+"/js");
                file.mkdir();
                file = new File(PATH_SITES+slideShow.getTitle()+"/img");
                file.mkdir();
                file = new File(PATH_SITES+slideShow.getTitle()+"/css");
                file.mkdir();
                
        } 
        catch (Exception e){
            e.printStackTrace();
        }
        file = new File(PATH_SITES+"/base/img");
        File[] images = file.listFiles();
        File file2;
        for(int i =0; i<images.length; i++){
            //String path = images[i].getPath();
            //Path source = Paths.get(path+"/img");
            //Path dest = Paths.get(PATH_SITES+slideShow.getTitle()+"/icon"+i);
            String name = images[i].getName();
            file2 = new File(PATH_SITES+slideShow.getTitle()+"/img/"+name);
            Files.copy(images[i].toPath(), file2.toPath(), StandardCopyOption.REPLACE_EXISTING);
        }
        for(int j = 0;j<slideShow.getSlides().size();j++){
            String name = slideShow.getSlides().get(j).getImageFileName();
            file = new File(slideShow.getSlides().get(j).getImagePath()+name);
            file2 = new File(PATH_SITES+slideShow.getTitle()+"/img/"+name);
            Files.copy(file.toPath(), file2.toPath(), StandardCopyOption.REPLACE_EXISTING);
        
        }
        Path destPath = Paths.get(PATH_SITES+slideShow.getTitle()+"/index.html");
        Files.copy(PATH_BASE_HTML, destPath, StandardCopyOption.REPLACE_EXISTING);
        destPath = Paths.get(PATH_SITES+slideShow.getTitle()+"/js/Slideshow.js");
        Files.copy(PATH_BASE_JS, destPath, StandardCopyOption.REPLACE_EXISTING);
        destPath = Paths.get(PATH_SITES+slideShow.getTitle()+"/css/slideshow_style.css");
        Files.copy(PATH_BASE_CSS, destPath, StandardCopyOption.REPLACE_EXISTING);
    }
    //todo - make other files
    //todo - make a function to copy files
   // Files.copy(source.toPath(), dest.toPath());

    /**
     * Uses the slide show data to reload all the components for
     * slide editing.
     * 
     * @param slideShowToLoad SLide show being reloaded.
     */
    public void reloadSlideShowPane(SlideShowModel slideShowToLoad) {
	slidesEditorPane.getChildren().clear();
	for (Slide slide : slideShowToLoad.getSlides()) {
	    SlideEditView slideEditor = new SlideEditView(slide,this);
            slide.setSev(slideEditor);
	    slidesEditorPane.getChildren().add(slideEditor);
            if(slideShowToLoad.getSelectedSlide().equals(slide))
                slideEditor.setEffects(slideShowToLoad.getSelectedSlide().getSev()); 
            //slideShowToLoad.getSelectedSlide().getEditView().se         
	}
    }
}