/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ssm.view;

import java.io.File;
import java.net.URL;
import javafx.collections.ObservableList;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.stage.Stage;
import properties_manager.PropertiesManager;
import static ssm.StartupConstants.DEFAULT_THUMBNAIL_WIDTH;
import static ssm.StartupConstants.ICON_NEXT;
import static ssm.StartupConstants.ICON_PREVIOUS;
import static ssm.StartupConstants.ICON_TITLE;
import static ssm.StartupConstants.PATH_ICONS;
import static ssm.file.SlideShowFileManager.SLASH;
import ssm.model.Slide;
import ssm.model.SlideShowModel;

/**
 *
 * @author Randhawa
 */
public class ViewSlideShow extends Stage {

    Scene scn;
    String selection;
    ObservableList<Slide> slides;
    ImageView imgView;
    Button backBtn;
    Button nextBtn;
    Label caption;
    int currIndex;

    public ViewSlideShow(SlideShowModel slideShow) {
        this.setTitle(slideShow.getTitle());
        currIndex = 0;
        slides = slideShow.getSlides();
        this.setMinHeight(650);
        this.setMinWidth(800);
        this.getIcons().add(new Image("https://d30y9cdsu7xlg0.cloudfront.net/png/49992-200.png"));

        VBox vbx = new VBox();
        imgView = new ImageView();

        HBox hbx = new HBox();
        backBtn = new Button();
        nextBtn = new Button();
        caption = new Label();
        caption.setAlignment(Pos.CENTER);
        
        hbx.getChildren().add(backBtn);
        hbx.getChildren().add(nextBtn);
        hbx.getChildren().add(caption);

        vbx.getChildren().add(imgView);
        vbx.getChildren().add(hbx);
        scn = new Scene(vbx);

        this.setScene(scn);

        PropertiesManager props = PropertiesManager.getPropertiesManager();
	String imagePath = "file:" + PATH_ICONS + ICON_PREVIOUS;
	Image buttonImage = new Image(imagePath);
	backBtn.setGraphic(new ImageView(buttonImage));
        
        imagePath = "file:" + PATH_ICONS + ICON_NEXT;
	buttonImage = new Image(imagePath);
	nextBtn.setGraphic(new ImageView(buttonImage));
        
        setImage(0);
        
        backBtn.setOnAction(e -> {
            currIndex--;
            setImage(currIndex);
        });

        nextBtn.setOnAction(e -> {
            currIndex++;
            setImage(currIndex);
        });
        this.showAndWait();
    }

    public void setImage(int index) {
        if (index >= 0 && index < slides.size()) {
            if(index==0)
                backBtn.setDisable(true);
            else
                backBtn.setDisable(false);
            if(index==slides.size()-1)
                nextBtn.setDisable(true);
            else
                nextBtn.setDisable(false);
            Slide slide = slides.get(index);
            String imagePath = slide.getImagePath() + SLASH + slide.getImageFileName();
            File file = new File(imagePath);
            try {
                // GET AND SET THE IMAGE
                URL fileURL = file.toURI().toURL();
                Image slideImage = new Image(fileURL.toExternalForm());
                imgView.setImage(slideImage);

                // AND RESIZE IT
                imgView.setFitWidth(800);
                imgView.setFitHeight(600);
            } catch (Exception e) {
                // @todo - use Error handler to respond to missing image
            }
            caption.setText(slide.getCaption());
        }
    }

}
